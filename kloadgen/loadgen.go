package main

import (
	"context"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"time"

	"bitbucket.org/dkolbly/logging"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/dkolbly/cli"
)

var log = logging.New("lgen")

func main() {

	app := &cli.App{
		Name:  "kloadgen",
		Usage: "Generate kafka load",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:    "size",
				Usage:   "how big of a test to run (duration@rate or total/duration or @rate for indefinite)",
				Aliases: []string{"s"},
				Value:   "10s@10",
			},
			&cli.IntFlag{
				Name:    "ramp",
				Usage:   "ramp time to max (seconds)",
				Aliases: []string{"r"},
			},
			&cli.StringFlag{
				Name:    "topic",
				Usage:   "topic to generate load for",
				Aliases: []string{"t"},
				Value:   "topic.test",
			},
		},
		Action: doLoadGen,
	}

	logging.SetHumanOutput(false, false, "DEBUG")
	logging.PrettyShowExtraFields()
	app.Run(os.Args)
}

type durationRateTest struct {
	rampDuration time.Duration
	duration     time.Duration
	rate         int
	count        uint64
	start        time.Time
	end          time.Time
	rampend      time.Time
	rampcount    uint64
	initial      bool
}

func (d *durationRateTest) SetRamp(dt time.Duration) {
	d.rampDuration = dt

	rampCount := uint64(0.5 * float64(d.rate) * dt.Seconds())

	/*fmt.Printf("ramp count is %d (0.5 * %d / %.1f = %.2f)\n",
	rampCount,
	d.rate,
	dt.Seconds(),
	0.5*float64(d.rate)*dt.Seconds())*/
	d.rampcount = rampCount

}

// note that we run in arrears, so this is about "catching up" to what we should have
// sent in the interval (t0, t1)
func (d *durationRateTest) MiniBurst(t0, t1 time.Time) int {
	if d.initial {
		d.rampend = t0.Add(d.rampDuration)
		d.start = t0
		d.end = t0.Add(d.duration)
		d.initial = false
	}

	if t0.After(d.end) {
		return -1
	}

	var nf float64

	ax := t1.Sub(d.rampend)
	if ax >= 0 {
		nf = float64(d.rampcount) + float64(d.rate)*ax.Seconds()
		/*fmt.Printf("at %.3f %.1f\n",
		t1.Sub(d.start).Seconds(),
		nf)*/
	} else {
		t := t1.Sub(d.start).Seconds()
		nf = t * t * 0.5 * float64(d.rate) / d.rampDuration.Seconds()

		/*fmt.Printf("at %.3f %.1f  t^2 = %.3f  (%d)*t^2 = %.1f\n",
		t,
		nf,
		t*t,
		d.rampcount,
		nf)*/
	}
	n := int64(nf)
	if n < int64(d.count) {
		panic("not monotonic")
		n = int64(d.count) + 1
	}

	p := d.count
	d.count = uint64(n)
	//fmt.Printf("target is %d ... coming from %d --> %d\n", n, p, n-int64(p))
	return int(n - int64(p))
}

func newDurationRateTest(d time.Duration, r int) *durationRateTest {
	return &durationRateTest{
		duration: d,
		rate:     r,
		initial:  true,
	}
}

var drTestSize = regexp.MustCompile(`^([0-9]+)([hms])@([0-9]+)$`)
var tdTestSize = regexp.MustCompile(`^([0-9]+)/([0-9]+)([hms])$`)
var indefTestSize = regexp.MustCompile(`^@([0-9]+)$`)

type TestProfile interface {
	SetRamp(dt time.Duration)
	MiniBurst(t0, t1 time.Time) int // how many to send to catch up from the last interval (nominal 0.1), -1=stop
}

func parseTestSize(s string) TestProfile {

	atoi := func(s string) int {
		n, _ := strconv.ParseInt(s, 10, 32)
		return int(n)
	}

	atod := func(num, units string) time.Duration {
		d := time.Duration(atoi(num))
		switch units[0] {
		case 'h':
			d *= time.Hour
		case 'm':
			d *= time.Minute
		case 's':
			d *= time.Second
		}
		return d
	}

	m := drTestSize.FindStringSubmatch(s)
	if m != nil {
		return newDurationRateTest(atod(m[1], m[2]), atoi(m[3]))
	}
	panic("unsupported")
}

func doLoadGen(c *cli.Context) error {
	ctx := context.Background()

	shape := parseTestSize(c.String("size"))
	if c.IsSet("ramp") {
		shape.SetRamp(time.Duration(c.Int("ramp")) * time.Second)
	}

	p, err := kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers": "localhost",
	})
	if err != nil {
		panic(err)
	}

	// Delivery report handler for produced messages
	go func() {
		for e := range p.Events() {
			switch ev := e.(type) {
			case *kafka.Message:
				if ev.TopicPartition.Error != nil {
					log.Errorf(ctx, "Delivery failed: %v",
						ev.TopicPartition)
				}
			}
		}
	}()

	topic := c.String("topic")

	prev := time.Now()

	burst := 0
	total := 0
	for {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		n := shape.MiniBurst(prev, now)
		if n < 0 {
			break
		}
		log.Debugf(ctx, "sending burst of %d", n)
		for i := 0; i < n; i++ {
			msg := fmt.Sprintf("At %s -- burst[%d] seq[%d]",
				time.Now().Format(time.RFC3339Nano),
				burst,
				i)
			p.Produce(&kafka.Message{
				TopicPartition: kafka.TopicPartition{
					Topic:     &topic,
					Partition: kafka.PartitionAny,
				},
				Value: []byte(msg),
			},
				nil)
			total++
		}
		burst++
		prev = now
	}
	log.Infof(ctx, "sent a total of %d", total)

	// Wait for message deliveries
	p.Flush(15 * 1000)
	return nil
}
