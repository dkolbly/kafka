# Performance Test Suite

## Parameters

Name | Description
-----+------------------------
NP   |  Number of Partitions
PSD  |  Partition standard deviation (0=all message in one part, +Inf=evenly distributed)
OFF  |  Max offer rate, msgs/sec
Q    |  Ingress queue depth
HQ   |  Handler queue depth
SQ   |  Spill queue depth
W    |  Number of spill workers (per consumer)
CON  |  Number of consumer instances per process
PROC |  Number of consumer processes


## Adjusting Partition Counts

```
docker exec -it kretire_kafka_1 /bin/sh

cd /opt/kafka_2.12-1.0.0

bin/kafka-topics.sh --zookeeper zookeeper:2181 --create \
     --partitions 7 --topic topic.test --replication-factor 1
```

### Examine State of Topic

```
docker exec -it kretire_kafka_1 /bin/sh /opt/kafka_2.12-1.0.0/bin/kafka-topics.sh  --zookeeper zookeeper:2181 --list
```

### Examine State of a Consumer Group

tough luck, I can't find anything that works with the new API

```
docker exec -it kretire_kafka_1 /bin/sh /opt/kafka_2.12-1.1.0/bin/kafka-consumer-groups.sh  --zookeeper zookeeper:2181 --describe --group fuzzgroup2
```

## Start Grafana

docker pull kamon/grafana_graphite
docker run -d --name grafana -p 80:80 -p 81:81 -p 8125:8125/udp -p 8126:8126 -p 2003:2003 kamon/grafana_graphite

log in with admin/admin
