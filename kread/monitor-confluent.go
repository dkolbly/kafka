package kread

import (
	"context"
	"sync/atomic"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

const verbose = false

const watermarkQueryTimeoutMillis = 1000

type statKeeper struct {
	cons     *Consumer
	active   []*HandlerQueue
	prevTime time.Time
	prev     []tpMonitorSample
}

func (sk *statKeeper) startStats(lst []*HandlerQueue) {
	ctx := context.Background()
	sk.active = lst
	sk.prevTime = time.Now()
	sk.prev = sk.cons.monitorStart(ctx, lst)
}

func (sk *statKeeper) finishStats() {
	sk.periodicStatsAndCommit()
	sk.prev = nil
	sk.active = nil
}

func (sk *statKeeper) periodicStatsAndCommit() {
	t := time.Now()
	dt := t.Sub(sk.prevTime)

	ctx := context.Background()
	latest := sk.cons.monitorSnap(ctx, sk.active)
	log := sk.cons.log
	stat := sk.cons.stat
	coms := make([]kafka.TopicPartition, len(latest))
	fl := make([]*HandlerQueue, 0, len(latest))
	numcom := 0
	changes := 0

	ms := &MonitorSample{
		StartTime: sk.prevTime,
		EndTime:   t,
	}

	for i, tms := range latest {
		mr := MonitorRecord{
			Topic:     tms.topic,
			Partition: tms.part,
			Last:      tms.last,
			Ingested:  int(tms.pushed - sk.prev[i].pushed),
			Committed: int(tms.acked - sk.prev[i].acked),
			InFlight:  int(tms.pushed - tms.acked),
		}
		if tms.last != int64(kafka.OffsetInvalid) {
			mr.Last = tms.last
		}
		if tms.err == nil {
			mr.Server = tms.high
		}
		ms.Records = append(ms.Records, mr)

		mr.logDiff(ctx, log, stat, &sk.prev[i], &tms)
		if tms.committable() {
			tms.prepCommit(ctx, log, &coms[numcom], dt)
			if tms.commit != sk.prev[i].commit {
				changes++
			}
			numcom++
			fl = append(fl, tms.of)
		}
	}
	if stat != nil {
		stat.Snapshot(ms)
	}

	coms = coms[:numcom]

	// note that we commit our offsets even when they haven't
	// changed in order to keep our offsets from expiring.
	// Otherwise, a quiet partition could lose its offsets,
	// potentially even if there is still data in the buffer
	// (c.f. https://issues.apache.org/jira/browse/KAFKA-3806)
	if len(coms) > 0 {
		if log != nil && changes > 0 {
			log.Debugf(ctx, "flushing %d commit changes", changes)
		}
		lst, err := sk.cons.client.CommitOffsets(coms)
		if err == nil {
			for i, q := range fl {
				if lst[i].Offset != coms[i].Offset {
					panic("wtf")
				}
				q.flushed = int64(lst[i].Offset) - 1
			}
		} else {
			if log != nil {
				log.Errorf(ctx, "Failed to commit: %s", err)
			}
		}
	}

	sk.prev = latest
	sk.prevTime = t
}

/*func (cx *Consumer) monitor(c *kafka.Consumer, ch <-chan []*HandlerQueue) {

	tick := time.NewTicker(5 * time.Second)
	ctx := context.Background()

	var current []*HandlerQueue

	first := true
	var prev time.Time
	for {
		select {
		case t := <-tick.C:
			if first {
				cx.monitor1(ctx, current, nil)
				first = false
			} else {
				dt := t.Sub(prev)
				cx.monitor1(ctx, current, &dt)
			}
			prev = t

		case lst := <-ch:
			current = lst
		}
	}
}*/

// a monitoring sample for a single topic-partition
type tpMonitorSample struct {
	of     *HandlerQueue
	done   chan struct{}
	topic  string
	part   int32
	low    int64
	high   int64
	err    error
	commit int64
	last   int64
	queued int
	pushed uint64
	popped uint64
	acked  uint64
}

func (tms *tpMonitorSample) fetch(c *kafka.Consumer) {
	defer close(tms.done)

	tms.low, tms.high, tms.err = c.QueryWatermarkOffsets(
		tms.topic,
		tms.part,
		watermarkQueryTimeoutMillis)
}

func (cx *Consumer) monitorStart(ctx context.Context, current []*HandlerQueue) []tpMonitorSample {
	mr := make([]tpMonitorSample, len(current))

	// snapshot the state of our queues
	for i, q := range current {
		// make sure we read these in this order to avoid
		// "backwash" where we think more things have been
		// committed than have been pushed because we read
		// pushed before committed.  Note that we read these
		// in the reverse causal order; that means, e.g., that
		// if we read a value for "acked", the record it
		// represents *must* already be represented in
		// "popped".  "popped" might have more in it if more things
		// have been popped since we read acked, but it will never
		// be less
		acked := atomic.LoadUint64(&q.acked)
		popped := atomic.LoadUint64(&q.popped)
		pushed := atomic.LoadUint64(&q.pushed)
		if acked > popped {
			panic("confused")
		}
		if popped > pushed {
			panic("confused")
		}
		mr[i] = tpMonitorSample{
			of:     q,
			topic:  q.topic.Topic,
			part:   int32(q.part),
			commit: atomic.LoadInt64(&q.commit),
			last:   atomic.LoadInt64(&q.last),
			pushed: pushed,
			popped: popped,
			acked:  acked,
		}
	}
	return mr
}

func (cx *Consumer) monitorSnap(ctx context.Context, current []*HandlerQueue) []tpMonitorSample {
	mr := make([]tpMonitorSample, len(current))

	// snapshot the state of our queues
	for i, q := range current {
		mr[i] = tpMonitorSample{
			of:     q,
			topic:  q.topic.Topic,
			part:   int32(q.part),
			commit: atomic.LoadInt64(&q.commit),
			last:   atomic.LoadInt64(&q.last),
			pushed: atomic.LoadUint64(&q.pushed),
			popped: atomic.LoadUint64(&q.popped),
			acked:  atomic.LoadUint64(&q.acked),
			done:   make(chan struct{}),
			queued: len(q.ch),
		}
		p := &mr[i]
		go p.fetch(cx.client.Consumer)
	}

	log := cx.log

	// wait for those fetches to finish, and do some logging along the way
	for _, tms := range mr {
		<-tms.done
		if tms.err != nil {
			if log != nil {
				log.Errorf(ctx, "%s[%d] could not get offsets: %s",
					tms.topic, tms.part, tms.err)
			}
		}
	}
	return mr
}

func (mr *MonitorRecord) logDiff(ctx context.Context, log Logger, stat Statter, from, tms *tpMonitorSample) {
	if tms.last == int64(kafka.OffsetInvalid) {
		// we've never processed anything on this
		// topic-partition, so nothing to report
		// (except that very fact, if so configured)
		if verbose {
			log.Debugf(ctx, "%s[%d] offsets are: %d %d ; we haven't processed anything",
				tms.topic,
				tms.part,
				tms.low,
				tms.high)
		}
		return
	}

	lag := tms.high - tms.last - 1

	// if logging is configured, log our current state
	if log != nil {
		if verbose || lag != 0 || tms.queued != 0 {
			log.Debugf(ctx, "%s[%d] offsets are: %d %d ; lagging %d behind, working on %d [%d %d]",
				tms.topic,
				tms.part,
				tms.low,
				tms.high,
				lag,
				tms.queued,
				tms.pushed,
				tms.popped)
		}
	}
}

func (tms *tpMonitorSample) committable() bool {
	return tms.commit != int64(kafka.OffsetInvalid)
}

func (tms *tpMonitorSample) prepCommit(ctx context.Context, log Logger, tp *kafka.TopicPartition, dt time.Duration) {
	q := tms.of
	more := tms.commit - q.flushed
	if more != 0 && log != nil {
		if q.flushed == int64(kafka.OffsetInvalid) {
			log.Debugf(ctx, "%s[%d] flushing commit %d ; %.1f/sec",
				tms.topic,
				tms.part,
				tms.commit,
				float64(more)/(float64(dt)/float64(time.Second)),
			)
		} else {
			log.Debugf(ctx, "%s[%d] flushing commit %d > %d ; %.1f/sec",
				tms.topic,
				tms.part,
				tms.commit,
				q.flushed,
				float64(more)/(float64(dt)/float64(time.Second)),
			)
		}
	}
	tp.Topic = &tms.topic
	tp.Partition = int32(tms.part)
	tp.Offset = kafka.Offset(tms.commit + 1)
}

type MonitorSample struct {
	// we flush monitor samples during rebalancing, so all topic
	// partitions are always covered by the same time interval
	StartTime time.Time
	EndTime   time.Time
	Records   []MonitorRecord
}

type MonitorRecord struct {
	Topic     string
	Partition int32
	Server    int64 // newest message on server
	Last      int64 // most recently ingested offset (-1 = none)
	Ingested  int   // number of messages ingested since last sample
	Committed int   // number of messages committed since last sample
	InFlight  int   // number of messages currently in flight (i. but not yet c.)
	Lag       int64 // current lag
}
