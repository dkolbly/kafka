package kread

import (
	"context"
	"sync/atomic"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

var trace = false

type underlyingDriver struct {
	*kafka.Consumer
}

func (c *Consumer) Run() {
	c.slurp(c.client.Consumer)
}

func (c *Consumer) Stop() {
	c.client.Unsubscribe()
}

func (s *setup) boot() error {
	// for the most part, see:
	//    https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md
	//
	cfg := &kafka.ConfigMap{
		"client.id":                       s.userAgent,
		"bootstrap.servers":               "localhost",
		"group.id":                        s.group,
		"auto.offset.reset":               "earliest",
		"go.events.channel.enable":        true,
		"go.application.rebalance.enable": true,
		"log.connection.close":            false, // these are mostly pointless(?)
		"enable.auto.commit":              false,
		"coordinator.query.interval.ms":   180000, // every 3 minutes
		// "security.protocol": "ssl",
		// "ssl.ca.location": "/tmp/foo",
		// "ssl.certificate.location": "/tmp/foo",
		// "ssl.key.location": "/tmp/foo",
		// note that ALL messages go over this one channel, so we have to
		// make sure its big enough that we don't have a head-of-line
		// blocking problem.  We fan out messages by partition anyway,
		// so it might not be a problem
		"go.events.channel.size": 100,
	}
	c, err := kafka.NewConsumer(cfg)
	if err != nil {
		return err
	}

	var topics []string
	for name := range s.cons.topics {
		topics = append(topics, name)
	}

	c.SubscribeTopics(topics, nil)

	s.cons.client = underlyingDriver{c}
	return nil
}

type topicConsumer struct {
	group   *Consumer
	Topic   string
	handler HandlerFunc
	queues  map[int]*HandlerQueue
}

type HandlerQueue struct {
	topic   *topicConsumer
	part    int
	last    int64
	pushed  uint64
	popped  uint64
	acked   uint64
	commit  int64
	flushed int64
	ch      chan<- *Message
}

func (cx *Consumer) bind(tp kafka.TopicPartition) *HandlerQueue {
	part := int(tp.Partition)
	t := cx.topics[*tp.Topic]
	if t == nil {
		panic("msg on unknown topic")
	}

	if q, ok := t.queues[part]; ok {
		return q
	}

	ch := make(chan *Message, cx.bufferDepth)
	q := &HandlerQueue{
		topic:   t,
		part:    part,
		ch:      ch, // store only send rights
		last:    int64(kafka.OffsetInvalid),
		flushed: int64(kafka.OffsetInvalid),
		commit:  int64(kafka.OffsetInvalid),
	}
	go q.process(ch) // pass only receive rights
	t.queues[part] = q
	return q
}

func (q *HandlerQueue) send(item *Message) {
	atomic.AddUint64(&q.pushed, 1)
	atomic.StoreInt64(&q.last, int64(item.Offset))
	q.ch <- item
}

func (q *HandlerQueue) process(ch <-chan *Message) {
	fn := q.topic.handler

	for item := range ch {
		atomic.AddUint64(&q.popped, 1)
		item.DispatchTime = time.Now()
		fn(item.ctx, item)
	}
}

func (cx *Consumer) slurp(c *kafka.Consumer) {

	log := cx.log

	ticker := time.NewTicker(5 * time.Second)
	defer ticker.Stop()

	sk := statKeeper{
		cons: cx,
	}

	ctx := context.Background()
	if log != nil {
		log.Debugf(ctx, "starting up for %d topics",
			len(cx.topics))
	}

	// boot up with some metadata

	for k := range cx.topics {
		md, err := cx.client.GetMetadata(&k, false, 1000)
		if err != nil {
			panic(err)
		}
		if tm, ok := md.Topics[k]; ok {
			log.Debugf(ctx, "got metadata for %q ; %d parts",
				k, len(tm.Partitions))
		} else {
			log.Errorf(ctx, "no metadata returned for %q", k)
		}
	}
out:
	for {
		select {
		case ev, ok := <-c.Events():
			if !ok {
				break out
			}
			{
				t := time.Now()

				ctx := context.Background()
				//log.Debugf(ctx, "received event %T", ev)

				switch ev := ev.(type) {
				case kafka.RevokedPartitions:
					if log != nil {
						for _, tp := range ev.Partitions {
							log.Debugf(ctx, "Revoked %s[%d]",
								*tp.Topic,
								tp.Partition)
						}
					}
					sk.finishStats()
					c.Unassign()
					ix := make(map[string]int, 1)
					for _, tp := range ev.Partitions {
						topic := *tp.Topic
						ix[topic] = ix[topic] - 1
					}
					if cx.stat != nil {
						cx.stat.Rebalance(ctx, ix)
					}

				case kafka.AssignedPartitions:
					if log != nil {
						for _, tp := range ev.Partitions {
							log.Debugf(ctx, "Assigned %s[%d]",
								*tp.Topic,
								tp.Partition)
						}
					}
					c.Assign(ev.Partitions)
					active := make([]*HandlerQueue, len(ev.Partitions))
					for i, tp := range ev.Partitions {
						active[i] = cx.bind(tp)
					}
					ix := make(map[string]int, 1)
					for _, tp := range ev.Partitions {
						topic := *tp.Topic
						ix[topic] = ix[topic] + 1
					}
					if cx.stat != nil {
						cx.stat.Rebalance(ctx, ix)
					}
					sk.startStats(active)

				case *kafka.Message:
					topic := *ev.TopicPartition.Topic
					msg := &Message{
						Topic:       topic,
						Partition:   int(ev.TopicPartition.Partition),
						Offset:      int64(ev.TopicPartition.Offset),
						Key:         ev.Key,
						Value:       ev.Value,
						KafkaTime:   ev.Timestamp,
						ArrivalTime: t,
					}
					mctx := cx.boot(ctx, msg)
					msg.ctx = mctx
					if log != nil && trace {
						log.Debugf(mctx, "Received %s[%d]@%d -- time lag %s",
							topic,
							ev.TopicPartition.Partition,
							ev.TopicPartition.Offset,
							time.Since(ev.Timestamp),
						)
					}

					msg.in = cx.bind(ev.TopicPartition)
					msg.in.send(msg)

				case kafka.PartitionEOF:
					if log != nil && trace {
						log.Debugf(ctx, "End of %s[%d]",
							*ev.Topic,
							ev.Partition)
					}
				default:
					if log != nil {
						log.Errorf(ctx, "Dunno what to do with %T", ev)
					}
				}
			}
		case <-ticker.C:
			sk.periodicStatsAndCommit()
		}
	}
	if log != nil {
		log.Debugf(context.Background(), "event loop finished")
	}
}
