package kread

import (
	"context"
	"fmt"
	"sync/atomic"
	"time"
)

// A Message represents a single message from Kafka.  The Done() method
// must be called in order to advance the consumer group's offset.
type Message struct {
	in           *HandlerQueue
	ctx          context.Context
	Topic        string
	Partition    int
	Offset       int64
	Key          []byte
	Value        []byte
	KafkaTime    time.Time // kafka's timestamp
	ArrivalTime  time.Time // the time that *we* received it
	DispatchTime time.Time // the time that we dispatched it for processing
}

func (msg *Message) String() string {
	return fmt.Sprintf("%s[%d]@%d", msg.Topic, msg.Partition, msg.Offset)
}

// Done marks this message processing as complete, allowing the partition's
// offset to advance to this point.
func (msg *Message) Done(ctx context.Context, err error) {

	q := msg.in
	stat := q.topic.group.stat
	if stat != nil {
		if err == nil {
			stat.Success(ctx, msg)
		} else {
			stat.Failure(ctx, msg, err)
		}
	}
	atomic.AddUint64(&q.acked, 1) // TODO generalize ACK
	atomic.StoreInt64(&q.commit, msg.Offset)
}
