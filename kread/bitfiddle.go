package kread

import (
	"sync"
)

type shortPendingAcks struct {
	lock    sync.Mutex
	cond    *sync.Cond
	start   int64
	acked   uint64
	pending uint64
}

func newspa(start int64) *shortPendingAcks {
	sp := &shortPendingAcks{
		start: start,
	}

	sp.cond = sync.NewCond(&sp.lock)
	return sp
}

// pend reserves a slot in the pending acks buffer
func (sp *shortPendingAcks) pend(k int64) {
	sp.lock.Lock()
	defer sp.lock.Unlock()

	// otherwise, see if we have room in the buffer
	for k >= sp.start+64 {
		// nope... wait for there to be room
		//fmt.Printf("pend(%d) waiting for there to be room\n", k)
		sp.cond.Wait()
	}
	//fmt.Printf("pend(%d) adding to buffer\n", k)

	mask := uint64(1) << uint(k-sp.start)
	sp.pending |= mask
}

// ack marks a slot as acked.  it panics if it wasn't pending
func (sp *shortPendingAcks) ack(k int64) {
	sp.lock.Lock()
	defer sp.lock.Unlock()

	if k < sp.start {
		panic("ack before window")
	}
	if k >= sp.start+64 {
		panic("ack after window")
	}
	mask := uint64(1) << uint(k-sp.start)
	if sp.pending&mask == 0 {
		panic("ack not pending")
	}

	// yay, it's a valid ack :P
	sp.acked |= mask
	//fmt.Printf("ack(%d) now %016x\n", k, sp.acked)

	// shift off the bottom (the common case is that there is only
	// one to shift, so no need to get fancy here)
	for sp.acked&1 == 1 {
		sp.acked >>= 1
		sp.pending >>= 1
		//fmt.Printf("shift off %d\n", sp.start)
		sp.start++
	}
	sp.cond.Signal()
}
