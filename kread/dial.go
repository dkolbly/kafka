package kread

import (
	"context"
	"errors"
)

type Consumer struct {
	client underlyingDriver
	topics      map[string]*topicConsumer
	bufferDepth int
	log         Logger
	stat        Statter
	boot        ContextBuilder
}

type setup struct {
	userAgent string
	cons      *Consumer
	spilling  bool
	statting  bool
	logging   bool
	group     string
	buffering bool
}

func Dial(ctx context.Context, opts ...Option) (*Consumer, error) {
	cfg := &setup{
		userAgent: "dkolbly/kread/0.1",
		cons: &Consumer{
			topics:      make(map[string]*topicConsumer),
			bufferDepth: 1,
		},
	}

	for _, opt := range opts {
		err := opt(ctx, cfg)
		if err != nil {
			return nil, err
		}
	}
	if cfg.cons.boot == nil {
		// fallback value
		cfg.cons.boot = func(ctx context.Context, msg *Message) context.Context {
			return ctx
		}
	}

	if cfg.group == "" {
		return nil, errors.New("no Group() specified")
	}

	err := cfg.boot()
	if err != nil {
		return nil, err
	}

	return cfg.cons, nil
}

