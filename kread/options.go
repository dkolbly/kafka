package kread

import (
	"context"
	"errors"
	"fmt"
)

// An Option is something that configures a consumer in the Dial()
// function.  The definition is private, so options can only be
// produced using one of the option constructors from this package.
type Option func(context.Context, *setup) error

// Logger defines the facilities we consume from a logger
type Logger interface {
	Debugf(context.Context, string, ...interface{})
	Errorf(context.Context, string, ...interface{})
}

// Spill is an option that sets up a "spill" queue, which
// is used when the per-topic-partition work queue is full
func Spill(n int) Option {
	return func(_ context.Context, s *setup) error {
		if s.spilling {
			return errors.New("Spill() already configured")
		}
		s.spilling = true
		return errors.New("Spill() not supported")
	}
}

// A HandlerFunc is a function that defines how an application wants
// to handle a message.
type HandlerFunc func(context.Context, *Message)

// Handle is an option that configures the handling of a specific
// topic.  The given handler function will be run in a topic-partition
// specific goroutine unless a spill pool is configured, in which case
// overflow events might be handled there.
func Handle(topic string, fn HandlerFunc) Option {
	return func(_ context.Context, s *setup) error {
		if s.cons.topics[topic] != nil {
			return fmt.Errorf("Topic %q already handled", topic)
		}
		s.cons.topics[topic] = &topicConsumer{
			group:   s.cons,
			Topic:   topic,
			handler: fn,
			queues:  make(map[int]*HandlerQueue),
		}
		return nil
	}
}

// A Statter defines the events we will emit
type Statter interface {
	// stats that we rebalanced things
	Rebalance(context.Context, map[string]int)
	// snapshot information about all the partitions we're managing
	Snapshot(*MonitorSample)
	// stats that a message was received from kafka
	//Received(context.Context, *Message)
	// stats that a message was dispatched to a handler function
	//Dispatched(context.Context, *Message)
	// stats that a handler function returned nil error
	Success(context.Context, *Message)
	// stats that a handler function returned non-nil error
	Failure(context.Context, *Message, error)
	// stats that a partition is lagging (lag may be 0)
	//Lag(context.Context, string, int, int64)
}

// Stat is an option that includes a stat hook.  Not strictly required,
// if supplied this will provide access to some useful stats.
func Stat(st Statter) Option {
	return func(_ context.Context, s *setup) error {
		if s.statting {
			return errors.New("Log() already configured")
		}
		s.statting = true
		s.cons.stat = st
		return nil
	}
}

// The Log option configures a logger.  If no logger is configured,
// then this library operates silently, although the underlying
// libraries may occasionally dump errors to stdout/stderr.
func Log(l Logger) Option {
	return func(_ context.Context, s *setup) error {
		if s.logging {
			return errors.New("Log() already configured")
		}
		s.logging = true
		s.cons.log = l
		return nil
	}
}

// Group sets the consumer group name for the consumer.  A consumer group
// defines a set of consumers that the server will distribute load across.
func Group(name string) Option {
	return func(_ context.Context, s *setup) error {
		if s.group != "" {
			return fmt.Errorf("Group(%q) already configured", s.group)
		}
		s.group = name
		return nil
	}
}

// Buffer sets the internal buffer depth for the consumer. This is the
// number of messages that can be queue for a given topic-partition
// before either creating backpressure or spilling over to the spill
// pool if that is configured.
func Buffer(k int) Option {
	return func(_ context.Context, s *setup) error {
		if s.buffering {
			return fmt.Errorf("Buffer(%q) already configured", s.group)
		}
		s.buffering = true
		s.cons.bufferDepth = k
		return nil
	}
}

// SetupContext sets the function that will set up the context for a newly
// arrived message.
func SetupContext(fn ContextBuilder) Option {
	return func(_ context.Context, s *setup) error {
		if s.cons.boot != nil {
			return errors.New("SetupContext() already configured")
		}
		s.cons.boot = fn
		return nil
	}
}

// A ContextBuilder is a function used to set up the context for
// a message that has arrived.
type ContextBuilder func(context.Context, *Message) context.Context
