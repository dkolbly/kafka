# Kafka Consumer

[![GoDoc](https://godoc.org/bitbucket.org/dkolbly/kafka/kread?status.svg)](https://godoc.org/bitbucket.org/dkolbly/kafka/kread)

This package exposes a simplified kafka consumer interface.  It uses
the confluent kafka go library, which in turn means you have to have
librdkafka installed along with the C toolchain in order to make use
of it.  That's unfortunate and painful, but sometimes worth the cost.
(If you are looking for a pure-Go kafka client, check out
[sarama](https://github.com/Shopify/sarama).)

## Quick Example

A minimal consumer looks something like this:

``` go
package main

import (
	"context"
	"fmt"

	"bitbucket.org/dkolbly/kafka/kread"
)

func main() {

	ctx := context.Background()

	c, err := kread.Dial(ctx,
		kread.Group("fuzzgroup2"),
		kread.Handle("funtest", myHandler),
	)

	if err != nil {
		panic(err)
	}
	c.Run()
}

func myHandler(ctx context.Context, item *kread.Message) error {
	fmt.Printf("Got message: %q\n", item.Value)
	return nil
}
```

## Concepts

### Handlers

One or more handlers should be specified in order to actually do
something useful.  Each handler specified is a mapping of a topic name
to a handler function.  Each partition of each topic is processed by a
separate go routine, so message order is preserved in invocations of
the handler (noting that Kafka only has a concept of ordering within a
single partition).  (There is exception if the **Spill()** option is used.)

Handlers are specifed with the **Handle()** option.

### Buffer Size

The normal buffer is what drives each handler go-routine.  The limit
on the buffer size bounds the delay between when a message arrives from
the driver and when it is processed by a handler function.

If a statter is configured, the **Received** stat is emitted when the
message arrives from the driver, and the **Dispatched** stat is emitted
when the message is sent to the handler (*i.e.*, just before invoking
the handler function)

### Behavior Under Panics

This library does not recover from panics in the handler.  Our
philosophy is that if a handler panics, you probably want to crash the
service and restart with a clean slate.  If the handler wants to
capture panics and soften the blow, that's fine -- you can just
transform it into an error return, which will get statted as a
**Failure**, but *note* if you fail to Ack a message then the offset
will never be updated and when the system eventually restarts, all the
messages from the unack'd one to the present will get reprocessed.

(That comment is not specific to panics.  For this reason, this
library itself is considering panicking if an excessive number of
unacked messages accumulate.  I'd be interested in use cases around
this.)

### Spill Buffer

This library makes it easy to deal with certain kinds of overload without
getting into unbounded concurrency.  The idea is an overflow "spill"
buffer that can distribute work across a *pool* of handler workers in a
non-order-preserving way, utilized when the normal buffer is full and
a new message arrives.

### Consumer Group

This library always uses Kafka consumer groups.  If a **Group** option
is not specified, for now we panic.  A reasonable default might be supplied.

### Security

Mutually authenticated TLS connections can be made to the kafka server
by specifying the SSL (or SSLFile) options(*)

 - SSL() and SSLFile()

### Progress

Offsets are flushed periodically to Kafka (every few seconds), so
there is a window during which a process crash will cause replayed
messages.  Generally, this library adopts a preference for "at least
once" delivery, though of course nothing prevents the application from
marking a message as **Done()** even though it has not actually done
whatever it is going to do with the message.

## Possible Problem Areas

### Commit Races

If we have a bunch of traffic in flight, we don't properly handle
rebalancing.  The old consumer might still do the work even though it
isn't going to commit the result (e.g., because it doesn't show up in
their consumer list by the time the application acks it).

I think this only produces wasted work / duplicates.

A worse case happens because we keep HandlerQueue's around; if a
worker loses a partition and then gets it back much later but then
doesn't process anything off it, it may commit back a very old offset

### Queue Depths

What is the use of having large buffers before the application?  Performance
testing needed.

### Performance Suite

Need a performance test suite, for that matter.  See ../kloadgen

### Caught Up

TODO: We can keep track of what fraction of time we spent "caught up",
meaning the time between when we get an EOF on a TopicPartition and
the time we receive a message on it.  Is this useful data?

