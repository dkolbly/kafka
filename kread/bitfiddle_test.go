package kread

import (
	"sync"
	"testing"
)

// try also:
//    go test -bench=Benchmark.* bitbucket.org/dkolbly/kafka/kread

// see how fast we can handle the "common case"
// on my laptop, this comes out to 106 ns per pend/ack; on my workstation,
// it's about 77 ns/op
func BenchmarkFiddlerCommon(b *testing.B) {
	p := newspa(20000)
	for i := 0; i < b.N; i++ {
		p.pend(20000 + int64(i)*3)
		p.ack(20000 + int64(i)*3)
		p.pend(20001 + int64(i)*3)
		p.ack(20001 + int64(i)*3)
		p.pend(20002 + int64(i)*3)
		p.ack(20002 + int64(i)*3)
	}
}

// on my laptop, this comes out to 97 ns per pend/ack, actually
// faster perhaps because its able to batch the commits together?
func BenchmarkFiddlerWindow(b *testing.B) {
	p := newspa(20000)
	for i := 0; i < b.N; i++ {
		p.pend(20000 + int64(i)*3)
		p.pend(20001 + int64(i)*3)
		p.pend(20002 + int64(i)*3)
		p.ack(20002 + int64(i)*3)
		p.ack(20001 + int64(i)*3)
		p.ack(20000 + int64(i)*3)
	}
}

// a full sync across goroutines comes out to about 190 ns/op (which
// includes the cost of sending over the channel between the two
// goroutines) although note that this amortizes potentially across
// the size of the window because each goroutine is probably bursting
// 64 operations at a time, so worst case is 64*190 ns/op or 12 usec/op,
// which is quite noticable.  On my workstation, it's about 145 ns/opt.
func BenchmarkFiddlerWaiting(b *testing.B) {
	p := newspa(30000)

	ch := make(chan int, 1000)

	// this roughly models how we think we'll be used -- that is,
	// we'll have a single producer calling pend() on a sequential
	// set of items, and multiple (in the case of a spill pool)
	// consumers calling ack()'s asynchronously
	go func() {
		defer close(ch)
		for i := 0; i < b.N; i++ {
			p.pend(30000 + int64(i))
			ch <- i
		}
	}()

	var wg sync.WaitGroup
	wg.Add(3)

	for j := 0; j < 3; j++ {
		go func() {
			defer wg.Done()
			for i := range ch {
				p.ack(30000 + int64(i))
			}
		}()
	}

	wg.Wait()
}

// how fast can we run the common case
func TestFiddlerBlast(t *testing.T) {
	p := newspa(20000)

	for i := 0; i < 1000; i++ {
		p.pend(20000 + int64(i))
		p.ack(20000 + int64(i))
	}

	if p.start != 21000 {
		t.Fatalf("expected start=21000 got %d", p.start)
	}
}

// how does it work with overlap < the window
func TestFiddlerInWindow(t *testing.T) {
	p := newspa(50000)

	var wg sync.WaitGroup
	wg.Add(4)

	go func() {
		defer wg.Done()
		for i := int64(0); i < 1000; i++ {
			p.pend(50000 + i*4)
			p.ack(50000 + i*4)
		}
	}()

	go func() {
		defer wg.Done()
		for i := int64(0); i < 1000; i++ {
			p.pend(50000 + i*4 + 1)
			p.ack(50000 + i*4 + 1)
		}
	}()

	go func() {
		defer wg.Done()
		for i := int64(0); i < 1000; i++ {
			p.pend(50000 + i*4 + 2)
			p.ack(50000 + i*4 + 2)
		}
	}()

	go func() {
		defer wg.Done()
		for i := int64(0); i < 1000; i++ {
			p.pend(50000 + i*4 + 3)
			p.ack(50000 + i*4 + 3)
		}
	}()

	wg.Wait()

	if p.start != 54000 {
		t.Fatalf("expected 54000, got %d", p.start)
	}
}

// what happens when we have more stuff coming in to pending than fit in the
// window (the pend'ers should wait)
func TestFiddlerOverflow(t *testing.T) {
}
