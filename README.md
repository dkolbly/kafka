# Kafka Utilities

This is a collection of kafka utilities to present a cleaner, more
idiomatic interface on top of the confluent kafka go API.

## kread - Kafka Consumer

A kafka consumer is available in the kread package.

A minimal consumer looks something like this:

``` go
package main

import (
	"context"
	"fmt"

	"bitbucket.org/dkolbly/kafka/kread"
)

func main() {

	ctx := context.Background()

	c, err := kread.Dial(ctx,
		kread.Group("fuzzgroup2"),
		kread.Handle("funtest", myHandler),
	)

	if err != nil {
		panic(err)
	}
	c.Run()
}

func myHandler(ctx context.Context, item *kread.Message) error {
	fmt.Printf("Got message: %q\n", item.Value)
	return nil
}
```

A **Group** must be configured because this library always makes use
of Kafka consumer groups.  And at least one topic must be configured
with **Handle** because, well, otherwise what's the purpose?
